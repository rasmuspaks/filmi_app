

window.onload = function() {
    if (navigator.onLine) {
    generateTopListing();
} else {
    $('#search-mini').textinput('disable');	
        
    }
}

$('#searchArea').submit(function () {
    searchMovies()
    return false;
});

function generateTopListing() {
    
    document.getElementById('content').innerHTML = ""
    var topMoviesApiCallUrl = "https://api.themoviedb.org/3/movie/top_rated?api_key=8fc63fb139eb3a9773a5dfcfabe74125&language=en-US&page=1"
    $.get(topMoviesApiCallUrl, function(data) {
        if (data.results.length == 0) {
               document.getElementById('content').innerHTML = '<h2> Puuduvad Tulemused </h2>'
            } else {
            var count = 0
            document.getElementById('content').innerHTML += '<h2 id="top_movies"> TOP FILMID </h2>'
            while (count < data.results.length) {
                var picture
                if (JSON.stringify(data.results[count].poster_path).replace(/\"/g, "") == "null") {
                    picture = '"notavailable.png"'
                } else {
                    picture = '"https://image.tmdb.org/t/p/w500' + JSON.stringify(data.results[count].poster_path).replace(/\"/g, "") +'"'
                }
                
                document.getElementById('content').innerHTML +=  '<a id="moviePageGenerator" onclick="generateMoviePage(' + JSON.stringify(data.results[count].id).replace(/\"/g, "") + ')"> <div data-role="content" id="search_movie" > ' +
           '<img src= ' + picture + '>' + 
       ' <h5 id = "info">' +  JSON.stringify(data.results[count].title).replace(/\"/g, "")  + ' </h5> '+
       ' <p id = "info">' +  JSON.stringify(data.results[count].release_date).replace(/\"/g, "")  +  '</p> </div> ' 
       count++
    
            }
        }
    });

    
}




    

function searchMovies() {
        document.getElementById('content').innerHTML = ""
        var keyword = document.getElementById('search-mini').value
        var apiCallUrl ='https://api.themoviedb.org/3/search/movie?api_key=8fc63fb139eb3a9773a5dfcfabe74125&language=en-US&query= ' 
        + keyword +'&page=1&include_adult=false'
        $.get(apiCallUrl, function (data) {
            if (data.results.length == 0) {
                 document.getElementById('content').innerHTML = '<h3> Puuduvad Tulemused </h3>'
            } else {
            var counter = 0
            while (counter < data.results.length) {
                var picture
                if (JSON.stringify(data.results[counter].poster_path).replace(/\"/g, "") == "null") {
                    picture = '"notavailable.png"'
                } else {
                    picture = '"https://image.tmdb.org/t/p/w500' + JSON.stringify(data.results[counter].poster_path).replace(/\"/g, "") +'"'
                }
           document.getElementById('content').innerHTML +=  '<a id="moviePageGenerator" onclick="generateMoviePage(' + JSON.stringify(data.results[counter].id).replace(/\"/g, "") + ')"> <div data-role="content" id="search_movie"> ' +
           '<img src= ' + picture + '>' + 
       ' <h5 id = "info">' +  JSON.stringify(data.results[counter].title).replace(/\"/g, "")  + ' </h5> '+
       ' <p id = "info">' +  JSON.stringify(data.results[counter].release_date).replace(/\"/g, "")  +  '</p> </div> </a> '

       counter++
            }
    }

});
        

}

function findIfMovieInLocalStorage(movieID) {
     if(typeof(localStorage) !=='undefined' && localStorage["watchedMovies"] !== undefined) {
        var moviesWatched = JSON.parse(localStorage["watchedMovies"])
        var found = false
     for(var i = 0; i < moviesWatched.result.length; i++){
         if (moviesWatched.result[i].id == movieID) {
             found = true
             break;
         }
     }
    }
    return found
}

function generateMovieTrailerKey(movieID) {
    var result;
    $.ajax(
        {
         type: 'GET',
         async: false,
         url: 'https://api.themoviedb.org/3/movie/'+ movieID +'/videos?api_key=8fc63fb139eb3a9773a5dfcfabe74125&language=en-US',
         success: function(trailersList) {
            var trailerKey;
        if (trailersList.results.length == 0) {
            trailerKey = "null"
        } else {

            for (movieIndex = 0; movieIndex < trailersList.results.length; movieIndex++) {
                if (JSON.stringify(trailersList.results[movieIndex].type) == '"Trailer"' && JSON.stringify(trailersList.results[movieIndex].site == '"Youtube"')){
                    trailerKey = JSON.stringify(trailersList.results[movieIndex].key).replace(/\"/g, "")
                    break;
                }
            }
        }
        result = trailerKey
         }
        });
    
    
    return result
}

function generateMoviePage(movieID) {
    var actorString = ""
    var moviePageApiCallUrl = 'https://api.themoviedb.org/3/movie/'+movieID +'?api_key=8fc63fb139eb3a9773a5dfcfabe74125&language=en-US'
    $.get(moviePageApiCallUrl, function(data) {
        document.getElementById('heading').innerHTML = JSON.stringify(data.title).replace(/\"/g, "") 
        var image;
          if (JSON.stringify(data.poster_path).replace(/\"/g, "") == "null") {
                    image = '"notavailable.png"'
                } else {
                    image = '"https://image.tmdb.org/t/p/w500' + JSON.stringify(data.poster_path).replace(/\"/g, "") +'"'
                }
                var castApiCallUrl = 'https://api.themoviedb.org/3/movie/'+ movieID + '/credits?api_key=8fc63fb139eb3a9773a5dfcfabe74125'
                $.get(castApiCallUrl, function(actors) {
                    var countActors = 0
                    if (actors.cast.length > 0) {
                    while (countActors < 4) {
                        actorString += JSON.stringify(actors.cast[countActors].name).replace(/['"]+/g, "") 
                        if(countActors == 3) {
                            actorString += "."
                        } else {
                            actorString += ", "
                        } 
                        countActors++;
                    }
                } else {
                    actorString = "UNAVAILABLE"
                }
                    var movieTitle = JSON.stringify(data.title).replace(/['"]+/g, "")
                    var movieOverview = JSON.stringify(data.overview).replace(/['"]+/g, "")
                    var poster_path = JSON.stringify(data.poster_path).replace(/['"]+/g, "")
                    var movieReleaseDate = JSON.stringify(data.release_date).replace(/['"]+/g, "")
                       document.getElementById('movieContent').innerHTML = ' <div data-role="content" id="center-content"> <img src= ' + image + '>  <table data-role="table"  class="ui-responsive" id="movieTable">' +
     '<thead> <tr> <th> Release data </th> <th> Cast </th> <th> Overview </th> </tr> </thead> <tbody> <tr> ' +
      ' <td> ' + movieReleaseDate  + ' </td> <td>' + actorString + ' </td> <td>' + movieOverview   +'</td> ' 
     + ' </tr>  </tbody> </table>   </div> '

     var found = findIfMovieInLocalStorage(movieID)
     var trailerKey = generateMovieTrailerKey(movieID)

    if (!found) {
        document.getElementById('movieContent').innerHTML += '<input type="button" id="addButton" data-role="button" data-icon="plus" onClick="watchingList('+movieID + ", '"+ movieTitle + "'," + "'" + movieOverview + "','" + actorString.replace(/['"]+/g, "") + "','" + movieReleaseDate + "','" + poster_path+"'" + ')" value="Add to watch list">'
    } else {
         document.getElementById('movieContent').innerHTML += '<input type="button" id="addButton" data-role="button" data-icon="delete"  onClick="watchingList('+movieID + ", '"+ movieTitle + "'," + "'" + movieOverview + "','" + actorString.replace(/['"]+/g, "") + "','" + movieReleaseDate + "','" + poster_path+"'" + ')" value="Delete from watch list">'
    }

    if (navigator.onLine && trailerKey != "null") {
        var videoSource = 'https://www.youtube.com/embed/' + trailerKey
        document.getElementById('movieContent').innerHTML += '<button> <a id="trailerButton" href="#popupVideo" data-rel="popup" data-position-to="window">Vaata trailerit</a> </button> ' +
'<div data-role="popup" id="popupVideo" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content"> <iframe src="' + videoSource +'"'+' width="250px" height="250px" seamless allowfullscreen="allowfullscreen"></iframe></div>'
        
        
        
        
        
    } 
    window.location.href = "#movie"

                });
                
                
     

    });

}

function generateMovieJson(movieID, movieTitle, movieOverview, movieActors, movieReleaseDate, poster_path) {
     return  {
            id: movieID,
            title: movieTitle,
            overview: movieOverview,
            actors: movieActors,
            release_date : movieReleaseDate,
            poster: poster_path,
        }
        
}

function watchingList(movieID, movieTitle, movieOverview, movieActors, movieReleaseDate, poster_path) {

        if($('#addButton').val() == "Add to watch list") {
        $('#addButton').attr('value', "Delete from watch list" )
        } else {
             $('#addButton').attr('value', "Add to watch list" )
        }
    
   
    if(typeof(localStorage) !=='undefined') {
        if (localStorage["watchedMovies"] === undefined) {
           var data = {
            result: [
                ]
        }
            localStorage["watchedMovies"] = JSON.stringify(data)
        }

        var foundMovie = false;
        var moviesWatched = JSON.parse(localStorage["watchedMovies"])
        var movieIndex = 0
       for(var i = 0; i < moviesWatched.result.length; i++){
           if(moviesWatched.result[i].id == movieID) {
               foundMovie = true;
               movieIndex = i
               break;

           }
       }
       if (!foundMovie) {
        var addMovie = generateMovieJson(movieID, movieTitle, movieOverview, movieActors, movieReleaseDate, poster_path)

        var watchedMovies = JSON.parse(localStorage["watchedMovies"])
        watchedMovies['result'].push(addMovie)
        localStorage["watchedMovies"] = JSON.stringify(watchedMovies)

       } else {
            var JSONtoDeleteFrom = JSON.parse(localStorage["watchedMovies"])
            JSONtoDeleteFrom.result.splice(movieIndex, 1)
            localStorage["watchedMovies"] = JSON.stringify(JSONtoDeleteFrom)
          
           }
        
    } else {
        alert("Update browser to add to watching list")
    }

}


function generateWatchedMovies() {
    if(localStorage["watchedMovies"] === undefined) {
         document.getElementById('movie-content').innerHTML = '<h3> Puuduvad filmid </h3>'
    } else {
    document.getElementById('movie-content').innerHTML = ""
        var count = 0
        var moviesWatched = JSON.parse(localStorage["watchedMovies"])
        while (count < moviesWatched.result.length) {
                var picture
                if (JSON.stringify(moviesWatched.result[count].poster).replace(/\"/g, "") == "null" || !navigator.onLine) {
                    picture = '"notavailable.png"'
                } else {
                    picture = '"https://image.tmdb.org/t/p/w500' + JSON.stringify(moviesWatched.result[count].poster).replace(/\"/g, "") +'"'
                }
                
                document.getElementById('movie-content').innerHTML +=  '<a id="moviePageGenerator" onclick="generateMoviePage(' + JSON.stringify(moviesWatched.result[count].id).replace(/\"/g, "") + ')"> <div data-role="content" id="search_movie" style="border: 1px solid black; "> ' +
           '<img src= ' + picture + '>' + 
       ' <h5 id = "info"> ' +  JSON.stringify(moviesWatched.result[count].title).replace(/\"/g, "")  + ' </h5> '+
       ' <p id = "info">' +  JSON.stringify(moviesWatched.result[count].release_date).replace(/\"/g, "")  +  '</p> </div> ' 
       
       count++
    }
            window.location.href ="#pagetwo"
        }
}
   






